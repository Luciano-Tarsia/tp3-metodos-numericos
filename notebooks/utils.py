import numpy as np
import metnum
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.model_selection import KFold

def RMSE(aprox, real):
    return np.sqrt(((aprox - real)**2).mean())

def RMSLE(aprox, real):
    return RMSE(np.log(np.abs(aprox) + 1), np.log(np.abs(real) + 1))

def correr_modelo(df_features,df_predict):
    
    X_train, X_test, y_train, y_test = train_test_split(df_features, df_predict, test_size=0.33, random_state=420)
    model = metnum.LinearRegression()
    model.fit(X_train, y_train)
    pred = model.predict(X_test)

    # Le cambiamos el formato a res
    pred = [item for sublist in pred for item in sublist]
    print(f"El error RMSE es {RMSE(pred,y_test)}")
    print('El error RMSLE es',RMSLE(pred,y_test))
    
    return

def kFold(K,sizeX):
    kfold = []
    aux = 0
    tamañoDelFold = np.ceil(sizeX/K)
    while(aux < sizeX):
        indice = aux+tamañoDelFold
        if(indice > sizeX):
            indice = sizeX
            
        indices = [int(aux),int(indice)]
        kfold.append(indices)
        aux += tamañoDelFold
    return kfold

def kFoldCrossValidation(X,y,K=5):
    
    kfold = kFold(K,X.shape[0])
    X_copy = X
    splitNumber = 1
        
    sumRMSE, sumRMSLE = 0, 0
    
    for i in range(len(kfold)):
        aux = kfold[i]
        #Separo el data set en datos de entrenamiento y de validacion
        X_test = X_copy[aux[0]:aux[1]]
        y_test = y[aux[0]:aux[1]]
        X_train = np.concatenate((X_copy[:aux[0]+1], X_copy[aux[1]:]), axis=0)
        y_train = np.concatenate((y[:aux[0]+1], y[aux[1]:]), axis=0)

        #Hago una prediccion con mi modelo
        model = metnum.LinearRegression()
        model.fit(X_train, y_train)
        pred = model.predict(X_test)

        #Le cambiamos el formato a pred y mostramos los errores
        pred = [item for sublist in pred for item in sublist]
        sumRMSE += RMSE(pred,y_test)
        sumRMSLE += RMSLE(pred,y_test)
        
        splitNumber += 1
    
    return [sumRMSE/K,sumRMSLE/K]