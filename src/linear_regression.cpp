#include <algorithm>
//#include <chrono>
#include <pybind11/pybind11.h>
#include <iostream>
#include <exception>
#include "linear_regression.h"

using namespace std;
namespace py=pybind11;

LinearRegression::LinearRegression() {}

void LinearRegression::fit(Matrix X, Matrix y) {
    Matrix A_normal = X.transpose() * X;
    Vector b_normal = X.transpose() * y;
    solution = A_normal.fullPivLu().solve(b_normal);
}


Matrix LinearRegression::predict(Matrix X) {
    return X * solution;
}
